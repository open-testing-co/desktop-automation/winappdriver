# WinAppDriver

This is a test automation example.
Windows Application Driver (WinAppDriver) is a service to support Selenium-like UI Test Automation on Windows Applications. This service supports testing Universal Windows Platform (UWP), Windows Forms (WinForms).

### Install & Run WinAppDriver
1. Download Windows Application Driver installer from <https://github.com/Microsoft/WinAppDriver/releases>
2. Run the installer on a Windows 10 machine where your application under test is installed and will be tested
3. Enable [Developer Mode](https://docs.microsoft.com/en-us/windows/uwp/get-started/enable-your-device-for-development) in Windows settings
4. Run `WinAppDriver.exe` from the installation directory (E.g. `C:\Program Files (x86)\Windows Application Driver`)

Windows Application Driver will then be running on the test machine listening to requests on the default IP address and port (`127.0.0.1:4723`). You can then run any of our [Tests](/Tests/) or [Samples](/Samples). `WinAppDriver.exe` can be configured to listen to a different IP address and port as follows:

```
WinAppDriver.exe 4727
WinAppDriver.exe 10.0.0.10 4725
WinAppDriver.exe 10.0.0.10 4723/wd/hub
```

> **Note**: You must run `WinAppDriver.exe` as **administrator** to listen to a different IP address and port.

### Find the ApplicationID
To get the names and Application ID for all apps installed for the current user, perform the following steps: 
1. Open Run *(Win + R)*, enter `shell:Appsfolder`, and select `OK`. A File Explorer window opens and shows all apps installed. 
2. Press **Alt** to show the toolbar with options `File, Edit, View, Tools` and go to View > Choose details...
![alt text](img/ApplicationId1.png "ApplicationId Step1")
3. In Choose Details window, select AppUserModelId, and then select OK. Check the Details option is selected in the View setting.
![alt text](img/ApplicationId2.png "ApplicationId Step2")
E.g. 
```
c24c8163-548e-4b84-a466-530178fc0580_scyf5npe3hv32!App
Microsoft.WindowsCalculator_8wekyb3d8bbwe!App
io.appium.desktop
```