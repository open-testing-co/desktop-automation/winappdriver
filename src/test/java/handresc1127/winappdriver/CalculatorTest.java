package handresc1127.winappdriver;

import io.appium.java_client.MobileBy;
import io.appium.java_client.windows.WindowsDriver;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

public class CalculatorTest {

	private static WindowsDriver<?> driver = null;
	private static final String app = "Microsoft.WindowsCalculator_8wekyb3d8bbwe!App";

	private static final By byResult = MobileBy.AccessibilityId("CalculatorResults");

	private static final By byClear = MobileBy.AccessibilityId("clearButton");
	private static final By byEqual = MobileBy.AccessibilityId("equalButton");

	private static final By byPlus = MobileBy.AccessibilityId("plusButton");
	private static final By byMinus = MobileBy.AccessibilityId("minusButton");
	private static final By byMultiply = MobileBy.AccessibilityId("multiplyButton");
	private static final By byDivide = MobileBy.AccessibilityId("divideButton");

	private static final By byNum0 = MobileBy.AccessibilityId("num0Button");
	private static final By byNum1 = MobileBy.AccessibilityId("num1Button");
	private static final By byNum2 = MobileBy.AccessibilityId("num2Button");
	private static final By byNum3 = MobileBy.AccessibilityId("num3Button");
	private static final By byNum4 = MobileBy.AccessibilityId("num4Button");
	private static final By byNum5 = MobileBy.AccessibilityId("num5Button");
	private static final By byNum6 = MobileBy.AccessibilityId("num6Button");
	private static final By byNum7 = MobileBy.AccessibilityId("num7Button");
	private static final By byNum8 = MobileBy.AccessibilityId("num8Button");
	private static final By byNum9 = MobileBy.AccessibilityId("num9Button");

	@BeforeAll
	static void initAll() {
		try {
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability("app", app);
			driver = new WindowsDriver(new URL("http://127.0.0.1:4723"), capabilities);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			assertNotNull(driver);
		} catch (Exception ex) {
			System.out.println("Make you sure the driver WinAppDriver.exe is running");
			ex.printStackTrace();
		}
	}

	@BeforeEach
	void init() {
		driver.findElement(byClear).click();
		assertEquals("0", _GetCalculatorResultText());
	}

	@Test
	void succeedingTest() {
	}

	@Test
	void passTest() {
		assertTrue(true);
	}

	@Test
	void failingTest() {
		fail("A fail");
	}

	@Test
	@Disabled("for demonstration purposes")
	void skippedTest() {
		// not executed
	}

	@Test
	void abortedTest() {
		assumeTrue("abc".contains("Z"));
		fail("test should have been aborted");
	}

	@Test
	public void Addition() {
		driver.findElement(byNum1).click();
		driver.findElement(byPlus).click();
		driver.findElement(byNum7).click();
		driver.findElement(byEqual).click();
		assertEquals("8", _GetCalculatorResultText());
	}

	@Test
	public void Combination() {
		driver.findElement(byNum7).click();
		driver.findElement(byMultiply).click();
		driver.findElement(byNum9).click();
		driver.findElement(byPlus).click();
		driver.findElement(byNum1).click();
		driver.findElement(byEqual).click();
		driver.findElement(byDivide).click();
		driver.findElement(byNum8).click();
		driver.findElement(byEqual).click();
		assertEquals("8", _GetCalculatorResultText());
	}

	@Test
	public void Division() {
		driver.findElement(byNum8).click();
		driver.findElement(byNum8).click();
		driver.findElement(byDivide).click();
		driver.findElement(byNum1).click();
		driver.findElement(byNum1).click();
		driver.findElement(byEqual).click();
		assertEquals("8", _GetCalculatorResultText());
	}

	@Test
	public void Multiplication() {
		driver.findElement(byNum9).click();
		driver.findElement(byMultiply).click();
		driver.findElement(byNum9).click();
		driver.findElement(byEqual).click();
		assertEquals("81", _GetCalculatorResultText());
	}

	@Test
	public void Subtraction() {
		driver.findElement(byNum9).click();
		driver.findElement(byMinus).click();
		driver.findElement(byNum1).click();
		driver.findElement(byEqual).click();
		assertEquals("8", _GetCalculatorResultText());
	}

	@AfterEach
	void tearDown() {
	}

	@AfterAll
	static void tearDownAll() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}
	}

	protected String _GetCalculatorResultText() {
		// trim extra text and whitespace off of the display value
		return driver.findElement(byResult).getText().replace("Display is", "").replace("Se muestra ", "").trim();
	}
}
